import React, { useState } from 'react';
import './styles/styles.css';

const Header = () => {
    const [openNav,setOpenNav] = useState(false);
    return ( 
        <>
        <div className={openNav?"h-auto bg-white pb-5 sm:navbar":"bg-white overflow-hidden h-12 sm:navbar sm:h-auto"}>
                <div className="brand ml-4 mt-2 sm:w-1/3 md:w-1/2 sm:brand">Brand</div>
                <div className="absolute top-0 right-0 mt-3 mr-4 cursor-pointer sm:hidden"  onClick={()=>setOpenNav(!openNav)}> 
                    <div className="menu-bar"></div>
                    <div className="menu-bar"></div>
                    <div className="menu-bar"></div>
                </div>
                <div className="text-center my-4 font-semibold text-gray-700 cursor-pointer hover:text-black sm:my-0 sm:nav-items">Menu 1</div>
                <div className="text-center my-4 font-semibold text-gray-700 cursor-pointer hover:text-black sm:my-0 sm:nav-items">Menu 2</div>
                <div className="text-center my-4 font-semibold text-gray-700 cursor-pointer hover:text-black sm:my-0 sm:nav-items">Menu 3</div>
                <div className="text-center my-4 font-semibold text-gray-700 cursor-pointer hover:text-black sm:my-0 sm:nav-items sm:special">Special</div>
            </div>
        </> 
     );
}
 
export default Header;