module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      screens:{
        'mobile':{'max' : '640px'},
      }
    },
  },
  variants: {},
  plugins: [],
  experimental : {
    applyComplexClasses :true
  }
}
